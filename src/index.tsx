//import './styles';
import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { reducer, defaultState } from './reducer';
import api from './api/middleware';
import createBrowserHistory from 'history/createBrowserHistory';
import { routerMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import * as containers from './containers';
import onclick from './onclick';
const history = createBrowserHistory({ keyLength: 8 });
const createStoreWithMiddleware = applyMiddleware(
  api,
  routerMiddleware(history),
  createLogger({
    logErrors: false,
    collapsed: true,
    diff: false,
    duration: false,
  })
)(createStore);
var my = [];
try {
  my = JSON.parse(localStorage.getItem('my')) || [];
} catch (e) {}
const initState = { ...defaultState, my };
const store = createStoreWithMiddleware(reducer, initState);
const main = document.createElement('main');
document.body.appendChild(main);
render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route path="/details/:id/" component={containers.PokeDetails} />
        <Route path="/my" component={containers.MyPokes} />
        <Route path="/:filter?" component={containers.PokeList} />
      </Switch>
    </Router>
  </Provider>,
  main
);
document.addEventListener('click', onclick(store.dispatch));
