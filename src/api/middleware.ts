export const BASE_URL = 'https://pokeapi.co/api/v2/pokemon';
export const BASE_IMG = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

function callApi(endpoint, config) {
  return fetch(`${BASE_URL}${endpoint}`, config).then(response => response.json());
}

export const CALL_API = 'CALL_API';

export default () => next => action => {
  // So the middleware doesn't get applied to every single action
  if (action.type !== CALL_API) {
    return next(action);
  }
  const { endpoint, types, options = {} } = action;
  // Passing the authenticated boolean back in our data will let us distinguish between normal and secret quotes
  const res = callApi(endpoint, options);
  if (types) {
    const [successType, errorType] = types;
    res.then(
      payload => {
        next({ type: successType, payload });
        return payload;
      },
      payload => {
        next({ error: payload.message, type: errorType, payload });
        return payload;
      }
    );
  }
  return res;
};
