import { CALL_API, BASE_URL } from './api/middleware';
export const POKE_LIST_FETCHED = 'POKE_LIST_FETCHED';
export const POKE_LIST_FAILED = 'POKE_LIST_FAILED';
export const POKE_DETAILS_FETCHED = 'POKE_DETAILS_FETCHED';
export const POKE_DETAILS_FAILED = 'POKE_DETAILS_FAILED';
export const ADD_TO_MY_LIST = 'ADD_TO_MY_LIST';
export const RM_FROM_MY_LIST = 'RM_FROM_MY_LIST';
export default {
  getPokeList,
  getPokeDetails,
};
export function getPokeList(next?) {
  return {
    type: CALL_API,
    endpoint: next ? next.replace(BASE_URL, '') : '/?',
    types: [POKE_LIST_FETCHED, POKE_LIST_FAILED],
  };
}
export function getPokeDetails(id) {
  return {
    type: CALL_API,
    endpoint: `/${id}`,
    types: [POKE_DETAILS_FETCHED, POKE_DETAILS_FAILED],
  };
}
export function addToMyList(payload) {
  return {
    type: ADD_TO_MY_LIST,
    payload,
  };
}
export function removeFromMyList(payload) {
  return {
    type: RM_FROM_MY_LIST,
    payload,
  };
}
