import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import { PokeList, Search, AppBar } from '../components';
import { getPokeList } from '../actions';
import { bind } from 'decko';
import Button from 'material-ui/Button';
import { CircularProgress } from 'material-ui/Progress';
import { Divider } from 'material-ui';

class PokeListContainer extends PureComponent<Props, State> {
  state = {
    results: [],
    filteredResults: null,
    isLoading: true,
  };
  @bind
  onSearchChange({ target: { value: _value } }) {
    const value = String(_value);
    if (!value) {
      return this.setState({ filteredResults: null });
    }
    const { results } = this.props;
    if (results) {
      const filteredResults = results.filter(({ name }) => name.includes(value));
      this.setState({ filteredResults });
    }
  }
  @bind
  getNextPokeList() {
    const { getPokeList, next } = this.props;
    getPokeList(next);
  }
  stopSubmit(e) {
    e.preventDefault();
  }
  componentDidMount() {
    const { results, getPokeList } = this.props;
    if (results.length === 0) {
      this.setState({ isLoading: true });
      getPokeList().then(() => this.setState({ isLoading: false }));
    } else {
      this.setState({ isLoading: false });
    }
  }

  render() {
    const { filteredResults, isLoading } = this.state;
    const { count, results, next, my } = this.props;
    return (
      <Fragment>
        <AppBar count={my.length} />

        <form onSubmit={this.stopSubmit}>
          <Search onChange={this.onSearchChange} />
          <Divider />
          {isLoading ? <CircularProgress /> : <PokeList my={my} count={count} results={filteredResults || results} />}
          {next && (
            <center>
              <Button variant="raised" color="primary" onClick={this.getNextPokeList}>
                next
              </Button>
            </center>
          )}
        </form>
      </Fragment>
    );
  }
}
//const setList =

export default connect(({ results, count, next, my }) => ({ results, count, next, my }), { getPokeList })(
  PokeListContainer as any
);

interface Props {
  next?: string;
  count: number;
  results: Result[];
  my: any[];
  getPokeList(a?: any): {};
}
interface Result {
  name: string;
  url: string;
}
interface State {
  filteredResults: Result[] | null;
}
