import PokeList from './PokeList';
import PokeDetails from './PokeDetails';
import MyPokes from './MyPokes';
export { PokeList, PokeDetails, MyPokes };
