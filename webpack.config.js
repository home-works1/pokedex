const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  resolve: {
    extensions: ['.tsx', '.ts', '.jsx', '.js', '.css'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: /\.\/src/,
        loader: 'buble-loader',
        query: {
          objectAssign: 'Object.assign',
        },
      },
      {
        test: /\.tsx?$/,
        include: /src/,
        use: [
          {
            loader: 'buble-loader',
            query: {
              objectAssign: 'Object.assign',
            },
          },
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
            },
          },
        ],
      },
      {
        test: /\.css?$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              namedExport: true,
              silent: true,
              context: 'src',
              localIdentName: '[path]--[name]--[local]',
              importLoaders: 1,
            },
          },
          {
            loader: 'postcss-loader',
            options: { config: { path: '.postcssrc.yml' } },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ template: 'src/index.html' }),
    new webpack.DefinePlugin({
      debug: 'console.debug',
      info: 'console.info',
      log: 'console.log',
      dir: 'console.dir',
      warn: 'console.warn',
      assert: 'console.assert',
      error: 'console.error',
      group: 'console.group',
      groupEnd: 'console.groupEnd',
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
  devServer: {
    historyApiFallback: true,
    overlay: {
      warnings: false,
      errors: true,
    },
  },
};
